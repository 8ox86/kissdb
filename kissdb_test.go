/*
 * =====================================================================================
 *
 *        Package:  kissdb
 *       Filename:  kissdb.go
 *
 *    Description:  go-kissdb
 *
 *        Created:  7/2/18 9:16 PM
 *
 *         Author:  hacklog
 *            URL:  http://80x86.io
 *
 * =====================================================================================
 */
package kissdb

import (
	"testing"
	"os"
	"strconv"
	"errors"
	"time"
	"fmt"
)

const hash_table_size = 10240
const key_size = 32
const value_size = 256
const TEST_FILE = "test.db"

//START HELPER

// Random number state.
// We generate random temporary file names so that there's a good
// chance the file doesn't exist yet - keeps the number of tries in
// TempFile to a minimum.
var rand uint32

func reseed() uint32 {
	return uint32(time.Now().UnixNano() + int64(os.Getpid()))
}

func nextSuffix() string {
	r := rand
	if r == 0 {
		r = reseed()
	}
	r = r*1664525 + 1013904223 // constants from Numerical Recipes
	rand = r
	return strconv.Itoa(int(1e9 + r%1e9))[1:]
}

func testFile() string {
	fname := nextSuffix() + ".db"
	return fname[:]
}
//END HELPER

func TestGetVersion(t *testing.T) {
	if ( 2 != GetVersion()) {
		t.Fail()
	}
}

var OPEN_MODE = KISSDB_OPEN_MODE_RWCREAT

func TestKissOpen(t *testing.T) {
	db := New()
	rs := Open(db, TEST_FILE, OPEN_MODE, hash_table_size, key_size, value_size)
	if (!rs) {
		t.Errorf("Open rs:%v\n", rs)
	}
	os.Remove(TEST_FILE)
}

func TestKissPutGet(t *testing.T) {
	db := New()
	rs := Open(db, TEST_FILE, OPEN_MODE, hash_table_size, key_size, value_size)
	if (!rs) {
		t.Errorf("Open rs:%v\n", rs)
	}
	rsP := Put(db, "a_short_key", "a_very_very_long_long_long_long_long_long_string")
	if (!rsP) {
		t.Errorf("Put")
	}
	var val string
	rsG := Get(db, "a_short_key", &val)
	if (!rsG) {
		panic(errors.New(fmt.Sprintf("TestKissGet.Get\n")))
	}
	if (val != "a_very_very_long_long_long_long_long_long_string") {
		panic(errors.New(fmt.Sprintf("val not eq\n")))
	}
	Close(db)
}

func TestKissGet(t *testing.T) {
	db := New()
	rs := Open(db, TEST_FILE, OPEN_MODE, hash_table_size, key_size, value_size)
	if (!rs) {
		t.Errorf("Open rs:%v\n", rs)
	}
	defer os.Remove(TEST_FILE)
	var val string
	rsG := Get(db, "a_short_key", &val)
	if (!rsG) {
		panic(errors.New(fmt.Sprintf("TestKissGet.Get\n")))
	}
	if (val != "a_very_very_long_long_long_long_long_long_string") {
		panic(errors.New(fmt.Sprintf("val not eq.\n")))
	}
	Close(db)
}

func BenchmarkOpen100(b *testing.B) {
	f := testFile()
	defer os.Remove(f)
	db := New()
	rs := Open(db, f, OPEN_MODE, hash_table_size, key_size, value_size)
	if (!rs) {
		panic(errors.New("open fail."))
	}

	for i := 1; i < 100; i++ {
		//b.Logf("Put: %d\t\t", i)
		Put(db, "hello:"+strconv.Itoa(i), "world"+strconv.Itoa(i))
	}
	Close(db)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		//b.Logf("bench Open: %d\t\t", i)
		db := New()
		rs := Open(db, f, OPEN_MODE, hash_table_size, key_size, value_size)
		if !rs {
			panic(errors.New("open fail."))
		}
		Close(db)
	}
}

func BenchmarkOpen1000(b *testing.B) {
	f := testFile()
	defer os.Remove(f)
	db := New()
	rs := Open(db, f, OPEN_MODE, hash_table_size, key_size, value_size)
	if (!rs) {
		panic(errors.New("open fail."))
	}

	for i := 1; i < 1000; i++ {
		//b.Logf("Put: %d\t\t", i)
		Put(db, "hello:"+strconv.Itoa(i), "world"+strconv.Itoa(i))
	}
	Close(db)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		//b.Logf("bench Open: %d\t\t", i)
		db := New()
		rs := Open(db, f, OPEN_MODE, hash_table_size, key_size, value_size)
		if !rs {
			panic(errors.New("open fail."))
		}
		Close(db)
	}
}

func BenchmarkGet(b *testing.B) {
	f := testFile()
	//result in that, on my machine: will open 4 files
	//b.Logf("file:%s\n",f )
	db := New()
	rs := Open(db, f, OPEN_MODE, hash_table_size, key_size, value_size)
	if (!rs) {
		panic(errors.New("open fail."))
	}
	defer os.Remove(f)

	//fill data first
	for i := 0; i < 10240; i++ {
		key := "hello:"+strconv.Itoa(i)
		val := "world"+strconv.Itoa(i)
		putRs := Put(db, key, val)
		if (!putRs) {
			panic(errors.New(fmt.Sprintf("BenchmarkGet.Put fail.\n")))
		}
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		var human string
		key := "hello:"+strconv.Itoa(i)
		getRs := Get(db, key, &human)
		if (!getRs) {
			//panic(errors.New(fmt.Sprintf("BenchmarkGet.Get fail. key:%s, file: %s\n", key, f)))
		}
	}
}

func BenchmarkSet(b *testing.B) {
	f := testFile()
	db := New()
	rs := Open(db, f, OPEN_MODE, hash_table_size, key_size, value_size)
	if (!rs) {
		panic(errors.New("open fail."))
	}
	defer os.Remove(f)
	b.ResetTimer()
	// set a key to any object you want
	for i := 0; i < b.N; i++ {
		rs := Put(db, "human:"+strconv.Itoa(i), "Human{\"Dante\", 5.4}")
		if rs != true {
			panic(errors.New("BenchmarkSet.Put fail."))
		}
	}
	Close(db)
}

func BenchmarkSave100(b *testing.B) {
	b.ResetTimer()
	for i := 1; i < b.N; i++ {
		f := testFile()
		db := New()
		rs := Open(db, f, OPEN_MODE, hash_table_size, key_size, value_size)
		if (!rs) {
			panic(errors.New("open fail."))
		}
		Put(db, "hello:"+strconv.Itoa(i), "world"+strconv.Itoa(i))
		Close(db)
		os.Remove(f)
	}
}

func BenchmarkSave10000(b *testing.B) {
	b.ResetTimer()
	for i := 1; i < b.N; i++ {
		f := testFile()
		db := New()
		rs := Open(db, f, OPEN_MODE, hash_table_size, key_size, value_size)
		if (!rs) {
			panic(errors.New("open fail."))
		}
		Put(db, "hello:"+strconv.Itoa(i), "world"+strconv.Itoa(i))
		Close(db)
		os.Remove(f)
	}
}
